<?php

return [
    'save.handler' => getenv('XHGUI_SAVE_HANDLER') ?: 'mongodb',
    'mongodb' => [
        'hostname' => getenv('XHGUI_MONGO_HOSTNAME') ?: '127.0.0.1',
        'port' => getenv('XHGUI_MONGO_PORT') ?: 27017,
        'database' => getenv('XHGUI_MONGO_DATABASE') ?: 'xhprof',
        'options' => [],
        'driverOptions' => [],
    ],
    'run.view.filter.names' => [
        'Zend*',
        'Composer*',
    ],
    'upload.token' => getenv('XHGUI_UPLOAD_TOKEN') ?: '',
    'path.prefix' => null,
    'timezone' => 'Europe/Berlin',
    'date.format' => 'Y-m-d H:i:s',
    'detail.count' => 6,
    'page.limit' => 25,
];