<?php

use Xhgui\Profiler\Profiler;
use Xhgui\Profiler\ProfilingFlags;

$mongoDsn = getenv('XHGUI_MONGO_DSN') ?? 'mongodb://127.0.0.1:27017';
$mongoDB = getenv('XHGUI_MONGO_DATABASE') ?? 'xhprof';

return [
    'profiler' => Profiler::PROFILER_TIDEWAYS_XHPROF,
    'profiler.flags' => [
        ProfilingFlags::CPU,
        ProfilingFlags::MEMORY,
        ProfilingFlags::NO_BUILTINS,
        ProfilingFlags::NO_SPANS,
    ],
    'save.handler' => Profiler::SAVER_MONGODB,
    'save.handler.mongodb' => [
        'dsn' => $mongoDsn,
        'database' => $mongoDB,
        'options' => [],
        'driverOptions' => [],
    ],
    'profiler.enable' => static function () {
        return true;
    },
];
