Feature:
    In order to prove that the Behat Symfony extension is correctly installed
    As a user
    I want to have a hello scenario

    Scenario: It receives a default response from Symfony's kernel
        When a hello scenario sends a request to "/hello"
        Then the response should be received
        And the response is JSON
        And the response has "salutation" with "Hello, World!"

    Scenario: It receives a response with name from Symfony's kernel
        When a hello scenario sends a request to "/hello/John"
        Then the response should be received
        And the response is JSON
        And the response has "salutation" with "Hello, John!"
