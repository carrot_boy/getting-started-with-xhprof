# Getting Started with XHProf

It is my attempt to play with XHProf profiler and UI for it.

## The easiest way to run:
- Install dependencies: `composer install`
- Run `docker-compose up -d`

## How to use
The app itself consists of 3 components:
- The simple API with only 1 endpoint: `/hello/{name}` (_name_ is optional). The API is running on 8081 port
- The Mongo DB for profiler data. The DB is not exposed for external usage by default but can be easily open
- The UI to represent profiler data - [XHProf](https://github.com/perftools/xhgui). The service is available via the 8142 port.

After the containers are up and ready, try to make a couple of requests to the endpoint and go to see the results.

For collecting the profiling data are 2 tools used:
- [Tideways XHProf Extension](https://github.com/tideways/php-xhprof-extension/)
- A [PHP library](https://github.com/perftools/php-profiler) to ease collecting

## Bonus: Tests
- Install dev dependencies (if they were not installed previously)
- Run command: `composer run tests` or `vendor/bin/behat`
